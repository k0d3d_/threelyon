import React from 'react'
import logoBeta from '../images/logo-beta.jpg'

const Header = () => (
        <header>
            <div class="container">
                <img src={logoBeta} alt="logo" class="header-logo" />
                <div class="float-right stripes-c">
                    <div class="pgra">

                    </div>
                    <div class="pgra">

                    </div>
                    <div class="pgra">

                    </div>
                </div>
            </div>
        </header>
)

export default Header
